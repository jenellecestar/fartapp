//
//  ViewController.swift
//  FartApp
//
//  Created by MacStudent on 2018-07-09.
//  Copyright © 2018 MacStudent. All rights reserved.
//



import UIKit
import AVFoundation

class ViewController: UIViewController {

    // variables and outlets
    
    // create a sound player variable
    var audioPlayer : AVAudioPlayer?
    
    // default functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // actions
    @IBAction func playFart(_ sender: UIButton) {
        
        var url = Bundle.main.url(forResource:"fart-03", withExtension:"mp3")
        
        if (sender.tag == 1) {
            print("WET FART PRESSED")
            url = Bundle.main.url(forResource:"fart-03", withExtension:"mp3")
        }
        else if(sender.tag == 2) {
            print("TOO MANY LENTILS PRESSED")
            url = Bundle.main.url(forResource:"fart-06", withExtension:"mp3")
        }
        else if(sender.tag == 3) {
            print("SQUEAKY FART PRESSED")
            url = Bundle.main.url(forResource:"fart-squeak-01", withExtension:"mp3")
        }
        
        
        
        // 1. creating a variable to represent the file location
        
        
        do {
            // code in here will throw errors
            // 2. tell the audio player the location of the file
            audioPlayer = try AVAudioPlayer(contentsOf: url!)
            
            // 3. play the sound
            audioPlayer?.play()
        }
        catch {
            // 4. deal with any errors!
            print("error while trying to play file")
        }
        
       
    }
    
    
}

